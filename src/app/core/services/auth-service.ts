import { Component, Injectable } from '@angular/core';
import { NgForm } from '@angular/forms';
import {Observable} from 'rxjs';
import {Data} from '@angular/router';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {SigninComponent} from '../../auth/signin/signin.component';
import {User} from '../models/user.model';

@Injectable()
export class AuthService {
  constructor(private _http: HttpClient) {
  }
  loggedIn: boolean;
  userLog: User;

  signupUser(form): Observable<Object> {
    return this._http.post( 'user', form.value);
  }

  signinUser(form): Observable<Object> {
    return this._http.post('login', form.value);
  }

  public getToken() {
    return localStorage.getItem('token');
  }

  signToken(): Observable<Object> {
    return this._http.get('login');
  }
  resetPassword(myForm): Observable<Object> {
    return this._http.post('mail', myForm);
  }
  updatePassword(myForm): Observable<Object> {
    return this._http.put('mail/' + 0, myForm);
  }

}
