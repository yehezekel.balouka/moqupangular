import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from './auth-service';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';

@Injectable()
export class UserService {
   constructor(private http: HttpClient, private authService: AuthService) {}

  updateUser(myForm): Observable<Object> {
    return this.http.put('user/' + this.authService.userLog.id, myForm);
  }
  getAllUsers(): Observable<object> {
    return this.http.get('user');
  }
  updateOtherUser(myForm, user: User): Observable<Object> {
    return this.http.put('user/' + user.id, myForm);
  }
}
