import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {AuthService} from './auth-service';
import {Injectable} from '@angular/core';
import {NgForm} from '@angular/forms';
import {Observable} from 'rxjs';
import {File} from '../models/file.model';


@Injectable()
export class FileService {
  constructor(private http: HttpClient, private authService: AuthService) {}
  getAllFiles(): Observable<object> {
    return this.http.get('files');
  }
  deleteFile(file: File): Observable<object> {
    return this.http.delete('files/' + file.id);
  }
  uploadFile(myFormData: FormData): Observable<object> {
    return this.http.post('files', myFormData);
  }
}
