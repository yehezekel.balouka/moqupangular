export class User {
  public first_name: string;
  public last_name: string;
  public email: string;
  public id: number;
  public role: string;

  // constructor(data) {
  //   this.first_name = data.first_name;
  //   this.last_name = data.last_name;
  //   this.email = data.email;
  //   this.id = data.id;
  //   this.role = data.role;
  // }
  constructor(data: Object) {
    Object.assign(this, data);
  }
}
