export class File {
  id: number;
  name: string;
  size: number;
  user_id: string;
  created_at: string;
  constructor(data: Object) {
    Object.assign(this, data);
  }
}
