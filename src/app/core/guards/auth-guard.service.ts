import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../services/auth-service';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Observable<boolean>(subscriber => {
      this.authService.signToken().subscribe(
        (data) => {
            this.router.navigate(['/home']);
            this.authService.userLog = new User(data);
            subscriber.next(false);
            subscriber.complete();
            },
        error => {
          subscriber.next(true);
          subscriber.complete();
        });
    });
  }
}
