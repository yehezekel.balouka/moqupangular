import { Injectable } from '@angular/core';
import {HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpClient, HttpHeaders} from '@angular/common/http';

import { Observable } from 'rxjs';
import {AuthService} from '../services/auth-service';
import {environment} from '../../../environments/environment';


/** Pass untouched request through to the next request handler. */
@Injectable()
export class InterceptorService implements HttpInterceptor {

  // urlStart = 'http://127.0.0.1:3000/api/';

  constructor(private authService: AuthService) {}
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (this.authService.getToken() !== null) {
      const secureReq = req.clone({
        url: environment.urlStart + req.url,
        setHeaders: { Authorization: `Bearer ${this.authService.getToken()}`}
      });
      return next.handle(secureReq);
    } else {
      const secureReq = req.clone({
        url: environment.urlStart + req.url,
      });
      return next.handle(secureReq);
    }
  }
}
