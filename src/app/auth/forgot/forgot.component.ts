import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/auth-service';
import {environment} from '../../../environments/environment';
import {Router} from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.css']
})
export class ForgotComponent implements OnInit {
  myForm
  constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      email: ['', Validators.email],
      url: environment.urlStart
    });
  }
  onSendEmail() {
    this.authService.resetPassword(this.myForm.value).subscribe(
      (data) => swal('we send to ' + this.myForm.value.email + ' reset password to his mail'),
      error1 => swal('the user ' + this.myForm.value.email + ' dont exist')
    );
  }

}
