import { Component, OnInit } from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../core/services/auth-service';

@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.css']
})
export class ResetComponent implements OnInit {

  token;
  constructor(private fb: FormBuilder, private activatedRoute: ActivatedRoute, private authService: AuthService, private router: Router) {
    this.activatedRoute.queryParams.subscribe(params => {
      this.token = params['token'];
    });
  }

  myForm;
  ngOnInit() {
    this.myForm = this.fb.group({
      email: [''],
      password: [''],
      password_confirmation: [''],
      token: this.activatedRoute.params['_value']['token']
    });
  }
  check() {
    return this.myForm['value']['password'] === this.myForm['value']['password_confirmation'];
  }
  updatePassword() {
    this.authService.updatePassword(this.myForm.value).subscribe(
      (data) => {swal('the user ' + data['original']['email'] + ' password was updated');
        this.router.navigate(['/signin']);
        },
      error1 => {
      swal(error1['error']['message']);
      }
    );
  }
}
