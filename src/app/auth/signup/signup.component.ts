import { Component, OnInit } from '@angular/core';
import {FormBuilder, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/auth-service';
import swal from 'sweetalert';
import {Router} from '@angular/router';
import {catchError} from 'rxjs/operators';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router, private fb: FormBuilder) { }

  myForm;
  ngOnInit() {
    this.myForm = this.fb.group({
      first_name: [''],
      last_name: [''],
      email: ['', Validators.email],
      password: [''],
      c_password: [''],
    });
  }

  onSignup() {
    this.authService.signupUser(this.myForm).subscribe(
      (data) =>  {
        if (data['original']['error'] === 'this email is already exist') {
          swal('this email is already exist');
        } else {
            swal('your create a new user: ' + this.myForm.value.email);
            this.router.navigate(['home']);
        }
      }, // success path
      error => {
        console.log(error.message); // error path
      }
    );
  }
  check() {
    return this.myForm['value']['password'] === this.myForm['value']['c_password'];
  }
}
