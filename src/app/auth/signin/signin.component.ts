import { Component, OnInit } from '@angular/core';
import {FormBuilder, NgForm, Validators} from '@angular/forms';
import {AuthService} from '../../core/services/auth-service';
import {Router} from '@angular/router';
import {AuthGuard} from '../../core/guards/auth-guard.service';
import {User} from '../../core/models/user.model';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  constructor(private authGuard: AuthGuard, private authService: AuthService, private router: Router, private fb: FormBuilder) { }
  myForm;
  ngOnInit() {
    this.myForm = this.fb.group({
      email: ['', Validators.email],
      password: [''],
    });
  }
  onSignin() {
    this.authService.signinUser(this.myForm).subscribe(
      (token: Response) => {
        if (token['original']['error'] === 'Unauthorised') {
          swal('error username or password !!!!!');
        } else {
            console.log(token['original']['user']);
            localStorage.setItem('token', token['original']['user']['token']);
            this.authService.userLog = new User(token['original']['user']);
            // console.log(this.authService.userLog);
            this.authService.loggedIn = true;
            this.router.navigate(['/home']);
        }
      },
      error => console.log(error.message)
      );
  }
  onForgot() {
    this.router.navigate(['forgot']);
  }
}
