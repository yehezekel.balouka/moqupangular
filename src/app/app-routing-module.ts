import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home/home.component';
import {SigninComponent} from './auth/signin/signin.component';
import {SignupComponent} from './auth/signup/signup.component';
import {NgModule} from '@angular/core';
import {AuthGuard} from './core/guards/auth-guard.service';
import {HomeGuard} from './core/guards/home-guard.service';
import {AuthModule} from './auth/auth-module';
import {ForgotComponent} from './auth/forgot/forgot.component';
import {ResetComponent} from './auth/reset/reset.component';

const appRoutes: Routes = [
  {path: 'signin', component: SigninComponent, canActivate: [AuthGuard]},
  {path: 'forgot', component: ForgotComponent, canActivate: [AuthGuard]},
  {path: 'reset/:token', component: ResetComponent, canActivate: [AuthGuard]},
  {path: 'signup', component: SignupComponent, canActivate: [AuthGuard]},
  {path: 'signout', redirectTo: '/signin'},
  {path: 'home', component: HomeComponent, canActivate: [HomeGuard], loadChildren: './home/home-module#HomeModule'},
  {path: '**', redirectTo: '/home'}
];

@NgModule({
  imports: [RouterModule.forRoot(appRoutes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
