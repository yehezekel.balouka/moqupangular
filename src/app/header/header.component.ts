import { Component, OnInit } from '@angular/core';
import {AuthGuard} from '../core/guards/auth-guard.service';
import {AuthService} from '../core/services/auth-service';
import {Router} from '@angular/router';
import {FileService} from '../core/services/file.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  isAuthenticated: boolean;
  constructor(private authGuard: AuthGuard, private authService: AuthService, private router: Router, private fileService: FileService) { }

  ngOnInit() {
  }

  onLogout() {
    localStorage.clear();
    this.authService.loggedIn = false;
    this.router.navigate(['/signin']);
  }
  check() {
    return this.authService.loggedIn;
  }


}
