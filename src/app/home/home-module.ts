import {NgModule} from '@angular/core';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {HomeRoutingModule} from './home-routing-module';

import {UsersComponent} from './users/users.component';
import {FilesComponent} from './files/files.component';
import {ProfilComponent} from './profil/profil.component';
import {CommonModule} from '@angular/common';


@NgModule({
  declarations: [
    UsersComponent,
    FilesComponent,
    ProfilComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HomeRoutingModule,
  ],
})
export class HomeModule { }
