import {RouterModule, Routes} from '@angular/router';
import {UsersComponent} from './users/users.component';
import {FilesComponent} from './files/files.component';
import {ProfilComponent} from './profil/profil.component';
import {NgModule} from '@angular/core';


const appHomeRoute: Routes = [
  {path: 'users', component: UsersComponent},
  {path: 'files', component: FilesComponent},
  {path: 'profil', component: ProfilComponent}
];


@NgModule({
  imports: [RouterModule.forChild(appHomeRoute)],
  exports: [RouterModule]
})

export class HomeRoutingModule {
}
