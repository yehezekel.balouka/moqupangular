import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AuthService} from '../../core/services/auth-service';
import {FormBuilder, NgForm} from '@angular/forms';
import {UserService} from '../../core/services/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  constructor(private authService: AuthService, private userService: UserService, private router: Router, private fb: FormBuilder) { }
  myForm = this.fb.group({
    first_name: [''],
    last_name: [''],
    email: [''],
  });
  ngOnInit() {
    this.myForm.patchValue({
      first_name: this.authService.userLog.first_name,
      last_name: this.authService.userLog.last_name,
      email: this.authService.userLog.email,
    });
  }
  onUpdate() {
    this.userService.updateUser(this.myForm.value).subscribe(
      (data: Response) => {
        console.log(data);
        this.authService.userLog.first_name = data['first_name'];
        this.authService.userLog.last_name = data['last_name'];
        this.authService.userLog.email = data['email'];
        swal('your user was updated');
        this.router.navigate(['/home']);
      },
      error => swal(error.message)
    );
  }
}
