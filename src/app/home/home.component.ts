import { Component, OnInit } from '@angular/core';
import {HomeGuard} from '../core/guards/home-guard.service';
import {AuthService} from '../core/services/auth-service';
import {Router} from '@angular/router';
import {AuthGuard} from '../core/guards/auth-guard.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) { }
  ngOnInit() {
  }
}
