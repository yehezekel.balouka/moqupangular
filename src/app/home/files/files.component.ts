import { Component, OnInit } from '@angular/core';
import {FileService} from '../../core/services/file.service';
import {File} from '../../core/models/file.model';
import {AuthService} from '../../core/services/auth-service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  constructor(private router: Router, private fileService: FileService, private authService: AuthService) { }
  files: File[];
  file: File;

  uploadFile: FileList;
  ngOnInit() {
    this.loadAllFiles();
  }
  loadAllFiles() {
    this.fileService.getAllFiles().subscribe(
      (data: Response) => {
        // console.log(data.success.allUsers);
        this.files = data['files'];
      }
    );
  }
  fileChange(event) {
    this.uploadFile = event.target.files[0];
  }
  onUpload () {
    const myFormData = new FormData();
    const myFile = new Blob([this.uploadFile]);
    myFormData.append('file', myFile, this.uploadFile['name']);
    myFormData.append('user_id', this.authService.userLog.id.toString());
    this.fileService.uploadFile(myFormData).subscribe(
      (data: Response) => {
        swal('the file was upload');
        this.loadAllFiles();
      },
      error1 => {console.log('you didnt success to sent an email to the administrator and upload the file '); }
    );
  }
  onDeleteFile(file: File, index: number) {
    this.fileService.deleteFile(file).subscribe(
      (data: Response) => {swal('the file ' + file.name + ' was deleted');
          this.files.splice(index, 1);
        },
      error1 => {swal('error you need to be an administrator!!!!!!'); }
    );
  }
}
