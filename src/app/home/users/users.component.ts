import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {UserService} from '../../core/services/user.service';
import {User} from '../../core/models/user.model';
import {AuthService} from '../../core/services/auth-service';
import {FormBuilder, FormControl, NgForm, Validators} from '@angular/forms';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @ViewChild('myTable') myTable: ElementRef;
  @ViewChild('editForm') editForm: ElementRef;
  constructor(private userService: UserService, private authService: AuthService, private fb: FormBuilder) {
  }
  users: User[];
  myForm = this.fb.group({
      first_name: [''],
      last_name: [''],
      email: [''],
    });
  user: User;
  index: number;
  ngOnInit() {
    this.userService.getAllUsers().subscribe(
      (data: User[]) => {
        // console.log(data.success.allUsers);
        this.users = data;
      },
      error => swal(error.message)
    );
    this.user = new User('');

  }
  editUser (user: User, index: number) {
    this.user = user;
    this.index = index;
    this.myForm.patchValue({
      first_name: user.first_name,
      last_name: user.last_name,
      email: user.email,
    });
    this.myTable.nativeElement.style.opacity = '0.1';
    this.editForm.nativeElement.style.display = 'block';
  }
  closeTemplate() {
    this.myTable.nativeElement.style.opacity = '1';
    this.editForm.nativeElement.style.display = 'none';
  }
  onUpdate() {
    console.log(this.myForm.value);
    this.userService.updateOtherUser(this.myForm.value, this.user).subscribe(
      (data: Response) => {
        swal('The user ' + this.myForm.value.email + ' was updated');
        this.users[this.index]['first_name'] = this.myForm.value['first_name'];
        this.users[this.index]['last_name'] = this.myForm.value['last_name'];
        this.users[this.index]['email'] = this.myForm.value['email'];
        this.closeTemplate();
      },
      error => swal(error.message)
    );
  }

}
